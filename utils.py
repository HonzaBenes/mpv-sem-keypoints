import numpy as np
import pandas as pd


def preprocess_train_file(filename):
    # load training file. It has a shape [5 x 31]
    # First 30 columns are x,y coordinates of facial keypoints
    # There are 15 keypoints = 30 features (one for 'x-coordinate' and one for 'y-coordinate')
    # 31st feature are image pixels (images are black and white)
    train_data = pd.read_csv(filename)

    # get column names
    # column_names = train_data.columns

    # check if there are some missing values in the dataset
    # train_data.isnull().any().value_counts()

    # there are sime missing values (keypoint coordinates) using 'ffill'.
    # (https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.fillna.html)
    train_data.fillna(method='ffill', inplace=True)

    # Next, seperate labels (=facial keypoint coordinates) and features (=image pixels)

    # get the image pixel values (= last column of train_data)
    # Pixel values from .csv file are strings, like this: '238 236 237 238 240 240 239 24'
    # You have to split that and convert to numpy array of float/int
    x_train = train_data.iloc[:, 30].apply(lambda pixel_row: np.asarray([int(i) for i in pixel_row.split(' ')]))
    x_train = np.vstack(x_train)

    # get labels (.values returns DataFrame values as numpy ndarray)
    y_train = train_data.iloc[:, 0:30].values

    return x_train, y_train
