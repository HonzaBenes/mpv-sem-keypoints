from env_dependent import DEVICE, DATA_DIR, EPOCHS


class Config:

    def __init__(self):
        self.DEVICE = DEVICE

        self.BATCH_SIZE = 32
        self.EPOCHS = EPOCHS

        self.MODEL_PATH = f'{DATA_DIR}/checkpoints/model.pth'

        self.LOOKUP = f'{DATA_DIR}/IdLookupTable.csv'

        self.TRAIN_SET = f'{DATA_DIR}/train.csv'
        self.TEST_SET = f'{DATA_DIR}/test.csv'

        self.PREDICTIONS = f'{DATA_DIR}/submission.csv'
