from torch.nn import Conv2d, LeakyReLU, BatchNorm2d, MaxPool2d, Linear, Dropout, Module


class Net(Module):

    def __init__(self):
        super(Net, self).__init__()
        # Block 1
        # input size 96x96x1
        self.conv1 = Conv2d(in_channels=1, out_channels=32, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky1 = LeakyReLU(0.1)
        self.b_norm1 = BatchNorm2d(32)

        # input size 96x96x32
        self.conv2 = Conv2d(in_channels=32, out_channels=32, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky2 = LeakyReLU(0.1)
        self.b_norm2 = BatchNorm2d(32)
        self.pool2 = MaxPool2d(kernel_size=(2, 2))

        # Block 2
        # input size 48x48x32
        self.conv3 = Conv2d(in_channels=32, out_channels=64, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky3 = LeakyReLU(0.1)
        self.b_norm3 = BatchNorm2d(64)

        # input size 48x48x64
        self.conv4 = Conv2d(in_channels=64, out_channels=64, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky4 = LeakyReLU(0.1)
        self.b_norm4 = BatchNorm2d(64)
        self.pool4 = MaxPool2d(kernel_size=(2, 2))

        # Block 3
        # input size 24x24x64
        self.conv5 = Conv2d(in_channels=64, out_channels=96, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky5 = LeakyReLU(0.1)
        self.b_norm5 = BatchNorm2d(96)

        # input size 24x24x96
        self.conv6 = Conv2d(in_channels=96, out_channels=96, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky6 = LeakyReLU(0.1)
        self.b_norm6 = BatchNorm2d(96)
        self.pool6 = MaxPool2d(kernel_size=(2, 2))

        # Block 4
        # input size 12x12x96
        self.conv7 = Conv2d(in_channels=96, out_channels=128, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky7 = LeakyReLU(0.1)
        self.b_norm7 = BatchNorm2d(128)

        # input size 12x12x128
        self.conv8 = Conv2d(in_channels=128, out_channels=128, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky8 = LeakyReLU(0.1)
        self.b_norm8 = BatchNorm2d(128)
        self.pool8 = MaxPool2d(kernel_size=(2, 2))

        # Block 5
        # input size 6x6x128
        self.conv9 = Conv2d(in_channels=128, out_channels=256, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky9 = LeakyReLU(0.1)
        self.b_norm9 = BatchNorm2d(256)

        # input size 6x6x256
        self.conv10 = Conv2d(in_channels=256, out_channels=256, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky10 = LeakyReLU(0.1)
        self.b_norm10 = BatchNorm2d(256)
        self.pool10 = MaxPool2d(kernel_size=(2, 2))

        # Block 6
        # input size 3x3x256
        self.conv11 = Conv2d(in_channels=256, out_channels=512, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky11 = LeakyReLU(0.1)
        self.b_norm11 = BatchNorm2d(512)

        # input size 3x3x512
        self.conv12 = Conv2d(in_channels=512, out_channels=512, kernel_size=(3, 3), padding=1, dilation=1, stride=1)
        self.leaky12 = LeakyReLU(0.1)
        self.b_norm12 = BatchNorm2d(512)

        # Flatten and get input
        # input size 3x3x512
        # self.flatten = Flatten()
        # input size 4608
        self.linear1 = Linear(in_features=4608, out_features=512)
        self.drop = Dropout(0.5)
        # input size 512
        self.linear2 = Linear(in_features=512, out_features=30)

    def forward(self, x):
        batch_size = x.shape[0]
        # Block 1
        x = self.conv1(x)
        x = self.leaky1(x)
        x = self.b_norm1(x)

        x = self.conv2(x)
        x = self.leaky2(x)
        x = self.b_norm2(x)
        x = self.pool2(x)

        # Block 2
        x = self.conv3(x)
        x = self.leaky3(x)
        x = self.b_norm3(x)

        x = self.conv4(x)
        x = self.leaky4(x)
        x = self.b_norm4(x)
        x = self.pool4(x)

        # Block 3
        x = self.conv5(x)
        x = self.leaky5(x)
        x = self.b_norm5(x)

        x = self.conv6(x)
        x = self.leaky6(x)
        x = self.b_norm6(x)
        x = self.pool6(x)

        # Block 4
        x = self.conv7(x)
        x = self.leaky7(x)
        x = self.b_norm7(x)

        x = self.conv8(x)
        x = self.leaky8(x)
        x = self.b_norm8(x)
        x = self.pool8(x)

        # Block 5
        x = self.conv9(x)
        x = self.leaky9(x)
        x = self.b_norm9(x)

        x = self.conv10(x)
        x = self.leaky10(x)
        x = self.b_norm10(x)
        x = self.pool10(x)

        # Block 6
        x = self.conv11(x)
        x = self.leaky11(x)
        x = self.b_norm11(x)

        x = self.conv12(x)
        x = self.leaky12(x)
        x = self.b_norm12(x)

        # Output
        x = x.reshape([batch_size, -1])
        # x = self.flatten(x)
        x = self.linear1(x)
        x = self.drop(x)
        x = self.linear2(x)

        return x
