import numpy as np
import pandas as pd
import torch

from config import Config


def preprocess_test_file(filename):
    test_data = pd.read_csv(filename)
    return test_data.Image.apply(lambda pixel_row: np.asarray([int(i) for i in pixel_row.split(' ')]))


def create_and_save_kaggle_submission(predictions, conf):
    lookid_data = pd.read_csv(conf.LOOKUP)
    lookid_list = list(lookid_data['FeatureName'])
    imageID = list(lookid_data['ImageId'] - 1)
    pred_list = list(predictions)

    rowid = list(lookid_data['RowId'])

    features = []
    for f in list(lookid_data['FeatureName']):
        features.append(lookid_list.index(f))

    loc_predictions = []
    for x, y in zip(imageID, features):
        loc_predictions.append(pred_list[x][y])

    rowid = pd.Series(rowid, name='RowId')
    loc = pd.Series(loc_predictions, name='Location')
    submission = pd.concat([rowid, loc], axis=1)

    submission.to_csv(conf.PREDICTIONS, index=False)


if __name__ == '__main__':
    conf = Config()
    model = torch.load(conf.MODEL_PATH, map_location=conf.DEVICE)

    x = preprocess_test_file(conf.TEST_SET)
    x = np.vstack(x.values)
    x = x.reshape(-1, 1, 96, 96)
    x = torch.from_numpy(x).float()

    with torch.no_grad():
        model.eval()
        out = model(x)

    predictions = out.numpy()
    create_and_save_kaggle_submission(predictions, conf)
