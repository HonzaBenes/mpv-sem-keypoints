import torch
from torch.nn import MSELoss
from torch.optim import Adam

from config import Config
from model import Net
from utils import preprocess_train_file


def get_next_batch(x, y, batch_size):
    for start, end in zip(range(0, x.shape[0] - batch_size, batch_size), range(batch_size, x.shape[0], batch_size)):
        yield x[start:end], y[start:end]


if __name__ == '__main__':
    conf = Config()

    # Load data
    # x_train: (7049, 9216), y_train: (7049, 30)
    x_train, y_train = preprocess_train_file(conf.TRAIN_SET)
    x_train = x_train.reshape(-1, 1, 96, 96)

    # Convert the data to torch tensors
    x_train, y_train = torch.from_numpy(x_train).float(), torch.from_numpy(y_train).float()

    model = Net()
    model = model.to(conf.DEVICE)

    # Define the loss and optimizer
    loss_func = MSELoss()
    optimizer = Adam(model.parameters())

    for epoch in range(conf.EPOCHS):
        print("Epoch: {}/{}".format(epoch + 1, conf.EPOCHS))

        # Set to training mode
        model.train()

        for i, (inputs, labels) in enumerate(get_next_batch(x_train, y_train, conf.BATCH_SIZE)):
            inputs = inputs.to(conf.DEVICE)
            labels = labels.to(conf.DEVICE)

            # Clean existing gradients
            optimizer.zero_grad()

            # Forward pass - compute outputs on input data using the model
            outputs = model(inputs)

            # Compute loss
            loss = loss_func(outputs, labels)

            # Backpropagate the gradients
            loss.backward()

            # Update the parameters
            optimizer.step()

            print(loss.item())

    torch.save(model, conf.MODEL_PATH)
